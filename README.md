# ELDAR CHALLENGE

## INTRODUCCION

Este un desarrollo challenge donde se propone dar soluciones a 5 problemas planteados, algunas de ellas a partir
del diseño de un sistema simple que permita procesar operaciones con tarjetas de credito para una organizacion.

Las consideraciones son las siguientes:
1. Una tarjeta se identifica de acuerdo a la marca, numero de tarjeta, cardHolder y fecha de vencimiento
2. Una operacion es valida si el consumo es menor a 1.000 pesos
3. Una tarjeta es valida para operar si la fecha de vencimiento es mayor al dia en curso
4. Existen tres marcas de tarjetas y cada una tiene su calculo propio de tasas.

## Parte 1
Se Requiere una clase ejecutable con 3 objetos que:
1. Invoque un metodo que devuelva la informacion de una tarjeta
2. Informar si una operacion es valida
3. Informar si una tarjeta es valida para operar
4. Identificar si una tarjeta es distinta a otra
5. Desarrollar un metodo que devuelva la tasa de una opreacion, indicando marca e importe

## Solucion 1
Como solucion a esta parte, se creo una clase ejecutable dentro del paquete com.demo.example.executable

## Parte 2
Desarrollar una API REST que devuelva la tasa de una opreacion, indicando marca e importe

## Solucion 2
Como solucion a esta parte, se creo un aplicativo dentro del paquete com.demo.example.demo.
Para probar el API REST se puede hacer ingresando a la URL del swagger http://localhost:8080/swagger-ui.html
Al levantar el aplicativo el application.properties esta configurado para crear el modelo de datos automaticamente,
bajo esta propiedad 
#####spring.jpa.hibernate.ddl-auto
por lo ue se recomienda levantar una base de datos local con postgresql
Una vez realizada esa parte, se recomienda ejecutar el siguiente comando de liquibase:
#####liquibase update --contexts=test
o ejecutar los script que se encuentran en el archivo
#####liquibase/changelog/insert_changelog.sql

## Notas
Se desarrollaron varias clases repositories, entidades las cuales no son necesarias, pero son un extra 
y representan un modelo de datos simple escalable.

## Parte 3
Se propone un ejercicio PL/SQL de seleccion simple
La respuesta seria la 1.

## Parte 4
Se propone un ejercicio PL/SQL de seleccion simple
La respuesta seria la 2.

## Parte 5
Se requiere desarrollar una funcion que dado un array de string, devuelva todos bajo un formato determinado

## Solucion 5
Como solucion este ejercicio, se desarrollo una clase ejecutable dentro del paquete com.example.extra


SIN MAS, SE DESPIDE MAURO, FUE UN GUSTAZO DEDICAR TIEMPO A RESOLVER ESTOS EJERCICIOS
CUALQUIER ACOTACION, COMENTARIO, CORRECCION O MEJORA, ENCANDO DE RECIBIR FEEDBACK