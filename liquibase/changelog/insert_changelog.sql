--liquibase formatted sql

--changeset mlisena:crb_card_brand_insert.0.1 context:dev,test
INSERT INTO demo.crb_card_brand (name) VALUES('VISA');
INSERT INTO demo.crb_card_brand (name) VALUES('NARA');
INSERT INTO demo.crb_card_brand (name) VALUES('AMEX');
-- rollback