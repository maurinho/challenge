package com.example.demo.dtos.request;

import lombok.*;

import javax.validation.constraints.NotNull;

import java.io.Serializable;

@Data
@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO implements Serializable {

    @NotNull(message = "")
    private Double amount;
    @NotNull(message = "")
    private CardDataDTO card;

    @Data
    @Builder
    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CardDataDTO implements Serializable {

        private String cardNumber;
        private String brand;
        private CardHolderDataDTO cardHolder;
        private String dueDate;


        @Data
        @Builder
        @Setter
        @Getter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class CardHolderDataDTO implements Serializable {

            private String name;
            private String lastName;
        }
    }
}
