package com.example.demo.repositories;

import com.example.demo.entities.card.CardBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardBrandRepository extends JpaRepository<CardBrand, Long> {

    Boolean existsByName(String name);
}
