package com.example.demo.controllers;

import com.example.demo.services.CardService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/card")
public class CardREST extends AbstractController {

    private CardService cardService;

    @PostMapping(value = "/cardBrand")
    public ResponseEntity<String> createCardBrand (
        @RequestHeader("brand") final String brand
    ) throws Exception {
        return cardService.insertCardBrand(brand);
    }
}