package com.example.demo.controllers;

import com.example.demo.dtos.request.TransactionDTO;
import com.example.demo.services.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/challenge")
public class ChallengeREST extends AbstractController {

    private TransactionService transactionService;

    @PostMapping()
    public ResponseEntity<String> validateAndGetTransactionData (
        @RequestBody final TransactionDTO transaction
    ) throws Exception {
        return transactionService.validateAndGetTransactionData(transaction);
    }
}
