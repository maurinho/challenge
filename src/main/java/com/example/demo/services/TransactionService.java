package com.example.demo.services;

import com.example.demo.dtos.request.TransactionDTO;
import lombok.AllArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class TransactionService {

    private CardService cardService;

    public ResponseEntity<String> validateAndGetTransactionData(
        TransactionDTO transaction
    ) throws Exception, ResponseStatusException {
        TransactionDTO.CardDataDTO card = transaction.getCard();
        cardService.validateCardData(card);
        validateTransactionData(transaction.getAmount());

        Double rate = getTransactionRate(card.getBrand());

        if (rate > 5.00) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El calculo de La tasa de la operacion es mayor a los limites maximos establecidos");
        }

        if (rate < 0.30) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El calculo de La tasa de la operacion es menor a los limites minimos establecidos");
        }

        JSONObject result = new JSONObject();
        result.put("cardBrand", card.getBrand());
        result.put("operationRate", rate);
        result.put("transactionAmount", transaction.getAmount());
        result.put("transactionTotal", String.valueOf(transaction.getAmount() + (transaction.getAmount() * (rate / 100))));
        return new ResponseEntity<>(result.toString(), HttpStatus.OK);
    }

    public void validateTransactionData(Double amount){
        if (amount < 0.01) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Monto erroneo");
        if (amount >= 1000) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Operacion invalida, monto superior al limite establecido");
    }

    private static Double getTransactionRate(String cardBrand){
        Double rate = null;
        LocalDate date = LocalDate.now();

        double day = date.getDayOfMonth();
        double month = date.getMonth().getValue();
        double year = Double.parseDouble(String.valueOf(date.getYear()).substring(2));

        switch(cardBrand) {
            case "VISA":
                rate = year / month;
                break;
            case "NARA":
                rate = day * 0.5;
                break;
            case "AMEX":
                rate = month * 0.1;
                break;
            default:
                break;
        }
        return rate;
    }
}
