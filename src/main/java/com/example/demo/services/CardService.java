package com.example.demo.services;

import com.example.demo.dtos.request.TransactionDTO;
import com.example.demo.entities.card.CardBrand;
import com.example.demo.repositories.CardBrandRepository;
import lombok.AllArgsConstructor;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@AllArgsConstructor
public class CardService {

    private static final String DATE_TIME_FORMAT = "dd/MM/yyyy";

    private CardBrandRepository cardBrandRepository;

    public Boolean isValidCard(String dueDate) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
        dueDate = "01/".concat(dueDate);
        return dateFormat.parse(dueDate).after(new Date());
    }

    public Boolean existsCardBrand(String brand) {
        return cardBrandRepository.existsByName(brand);
    }

    public ResponseEntity<String> insertCardBrand(
        String brand
    ) throws Exception {
        try {
            brand = brand.toUpperCase();
            Boolean exists = cardBrandRepository.existsByName(brand);
            if (Boolean.FALSE.equals(exists)) {
                CardBrand cardBrand = new CardBrand();
                cardBrand.setName(brand);
                cardBrandRepository.save(cardBrand);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Hubo un error en la peticion");
        }
    }

    public void validateCardData(TransactionDTO.CardDataDTO card) throws ParseException {
        String cardNumber = card.getCardNumber();
        String cardBrand = card.getBrand();
        String cardDueDate = card.getDueDate();

        if (!StringUtils.isNumeric(cardNumber) || cardNumber.length() != 16)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tarjeta erronea");

        /*
         * Ejecutar desde liquibase o manualmente sentencias que se encuentran el archivo insert_changelog.sql
         *
         * */
        if (Boolean.FALSE.equals(existsCardBrand(cardBrand)))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Marca de tarjeta Invalida o no existente");
        if (Boolean.FALSE.equals(isValidCard(cardDueDate)))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tarjeta Invalida");
    }
}
