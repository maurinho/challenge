package com.example.extra;

import java.util.regex.Pattern;

public class Exercise5 {

    private static final String REGEX = "/^[a-zA-Z0-9\\s]*$/";

    public static void main(String[] args) throws Exception {

        String[] my_array = {"FirstWord", "SecondWord", "THIRDWORD"};
        String strOut = bindStrings(my_array);

        System.out.println(strOut);

    }

    public static String bindStrings(String[] strArr) throws Exception {

        StringBuilder strOut = null;

        if (strArr.length > 10) {
            throw new Exception("La longitud de la matriz no debe ser mayor a 10");
        }

        for (String s : strArr) {
            if (Pattern.matches(REGEX, s)) {
                throw new Exception("El elemento no cumple con las caracteristicas");
            }

            if (strOut != null) {
                strOut.append(" ").append(s.toLowerCase());
            } else {
                strOut = new StringBuilder(s.toLowerCase());
            }
        }
        return strOut.toString();
    }
}
