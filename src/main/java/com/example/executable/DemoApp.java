package com.example.executable;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.plexus.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DemoApp {

    private static Gson gson = new Gson();
    private static final String DATE_TIME_FORMAT = "dd/MM/yyyy";

    public static void main(String[] args) throws Exception {

        Card card1 = new Card();
        card1.setCardHolder("Mauro Lisena");
        card1.setBrand("VISA");
        card1.setCardNumber("4111907665421234");
        card1.setDueDate("06/2025");

        Card card2 = new Card();
        card2.setCardHolder("Mario Lisena");
        card2.setBrand("NARA");
        card2.setCardNumber("4098541312110098");
        card2.setDueDate("02/2022");

        Card card3 = new Card();
        card3.setCardHolder("Enrique Perez");
        card3.setBrand("AMEX");
        card3.setCardNumber("5241099987670012");
        card3.setDueDate("09/2028");

        System.out.println("Tarjeta #1");
        System.out.println("Informacion de la tarjeta: " + getCardInfo(card1));
        System.out.println("Validez de una operacion: " + card1.getIsValidOperation(990.00));
        System.out.println("Validez de la tarjeta: " + card1.getIsValidCard());

        System.out.println("Tarjeta #2");
        System.out.println("Informacion de la tarjeta: " + getCardInfo(card2));
        System.out.println("Validez de una operacion: " + card2.getIsValidOperation(1200.18));
        System.out.println("Validez de la tarjeta: " + card2.getIsValidCard());

        System.out.println("Tarjeta #3");
        System.out.println("Informacion de la tarjeta: " + getCardInfo(card1));
        System.out.println("Validez de una operacion: " + card3.getIsValidOperation(541.97));
        System.out.println("Validez de la tarjeta: " + card3.getIsValidCard());

        //Comparar tarjetas
        System.out.println("Compracion de tarjetas: " + compareCards(card1, card2));

        //Exercise #1
        System.out.println("Informacion de una operacion: " + getTransactionInfo(card1, 900.10));
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Card {

        String brand;
        String cardNumber;
        String dueDate;
        String cardHolder;

        private String getIsValidCard() throws ParseException {
            return (isValidCard(getDueDate())) ? "Tarjeta valida para operar" : "Tarjeta Invalida";
        }

        private String getIsValidOperation(Double amount) {
            return (isValidOperation(amount)) ? "Operacion valida" : "Operacion invalida";
        }

    }

    private static Double validateAndGetTransactionData(Card card, Double amount) throws Exception {

        if (amount < 0.01) throw new Exception("Operacion invalida, monto invalido");
        if (!StringUtils.isNumeric(card.getCardNumber()) && card.getCardNumber().length() != 16) throw new Exception("Tarjeta erronea");
        if (Boolean.FALSE.equals(isValidOperation(amount))) throw new Exception("Operacion invalida, consumo mayor a 1.000 pesos");
        if (Boolean.FALSE.equals(isValidCard(card.getDueDate()))) throw new Exception("Tarjeta invalida para operar");

        Double transactionRate = getTransactionRate(card.getBrand());

        if (transactionRate > 5.00) throw new Exception("El calculo de La tasa de la operacion es mayor a los limites maximos establecidos");
        if (transactionRate < 0.30) throw new Exception("El calculo de La tasa de la operacion es menor a los limites minimos establecidos");

        return transactionRate;
    }

    private static String getTransactionInfo(Card card, Double amount) throws Exception {

        Double rate = validateAndGetTransactionData(card, amount) / 100;

        Map<String, String> result = new HashMap<>();
        result.put("cardBrand", card.getBrand());
        result.put("operationRate", rate.toString());
        result.put("transactionAmount", amount.toString());
        result.put("transactionTotal", String.valueOf(amount + (amount * rate)));
        return gson.toJson(result);
    }

    private static String getCardInfo(Card card) {
        return gson.toJson(card);
    }

    private static Boolean isValidCard(String dueDate) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
        dueDate = "01/".concat(dueDate);
        return dateFormat.parse(dueDate).after(new Date());
    }

    private static Boolean isValidOperation(Double amount){
        return (amount < 1000);
    }

    private static Double getTransactionRate(String cardBrand){
        Double rate = null;
        LocalDate date = LocalDate.now();

        double day = date.getDayOfMonth();
        double month = date.getMonth().getValue();
        double year = Double.parseDouble(String.valueOf(date.getYear()).substring(2));

        switch(cardBrand) {
            case "VISA":
                rate = year / month;
                break;
            case "NARA":
                rate = day * 0.5;
                break;
            case "AMEX":
                rate = month * 0.1;
                break;
            default:
                break;
        }
        return rate;
    }

    private static Boolean compareCards(Card card1, Card card2){
        return (card1.equals(card2));
    }
}